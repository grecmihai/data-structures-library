#include <stdlib.h>
#include <stdio.h>
#include "cclist.h"
#include "ccvector.h"
#include "cchashtable.h"
#include "ccheap.h"
#include "cctree.h"
void printList(CC_LIST_ENTRY *list);
void printVector(CC_VECTOR *vector);
void printTree(CC_TREE* Tree);
int main() {
	
	CC_LIST_ENTRY *list = NULL;
	int res = LstCreate(&list);
	if (res < 0) {
		printf("gogule probleme");
		return -1;
	}
	LstInsertValue(list, 1);
	printf("NodeCount pe lista cu un element:%d\n", LstGetNodeCount(list));
	LstInsertValue(list, 2);
	LstInsertValue(list, 3);
	printf("Test pentru insert:");
	printList(list);
	printf("NodeCount:%d\n", LstGetNodeCount(list));
	LstClear(list);
	printf("NodeCount pe lista vida:%d\n", LstGetNodeCount(list));
	CC_LIST_ENTRY *second_node = NULL;
	LstInsertValue(list, 4);
	LstInsertValue(list, 5);
	printList(list);
	LstGetNthNode(list, &second_node,2);
	int value = 0;
	LstGetNodeValue(second_node, &value);
	printf("second node value=%d\n", value);
	LstInsertValue(list, 6);
	LstInsertValue(list, 7);
	LstInsertValue(list, 8);
	printList(list);
	LstGetNthNode(list, &second_node, 1);
	LstRemoveNode(list, second_node);
	printList(list);
	LstGetNthNode(list, &second_node, 3);
	LstRemoveNode(list, second_node);
	printList(list);
	LstGetNthNode(list, &second_node, 3);
	LstRemoveNode(list, second_node);
	printList(list);
	LstClear(list);
	CC_LIST_ENTRY *list2 = NULL;
	LstCreate(&list2);
	LstInsertValue(list2, 2);
	LstInsertValue(list2, 5);
	LstInsertValue(list2, 12);
	LstInsertValue(list2, 25);
	printList(list2);
	printf("---------\n");
	LstMergeSortedLists(list, list2);
	printList(list);
	LstClear(list);
	LstInsertValue(list, 3);
	LstInsertValue(list, 7);
	LstInsertValue(list, 12);
	LstInsertValue(list, 19);
	LstInsertValue(list, 25);
	LstInsertValue(list, 27);
	LstMergeSortedLists(list, list2);
	printList(list);
	CC_LIST_ENTRY *list3 = NULL;
	LstCreate(&list3);
	LstInsertValue(list3, 27);
	LstInsertValue(list3, 38);
	LstInsertValue(list3, 3);
	LstInsertValue(list3, 43);
	LstInsertValue(list3, 9);
	LstInsertValue(list3, 82);
	LstInsertValue(list3, 10);
	printList(list3);
	LstSortByValues(list3);
	printList(list3);
	CC_LIST_ENTRY *node = NULL;
	printf("Lista pe care facem testele:");
	printList(list);
	LstGetNthNode(list, &node, 0);
	printf("GetNthNode pt primul element=%d\n", node->value);
	res = LstGetNthNode(list, &node, 12);
	if (res < 0) {
		printf("Invalid index\n");
	}
	else {
		printf("NthNode doesn't work\n");
	}
	LstDestroy(&list);
	res = LstCreate(&list);
	if (res > -1) {
		printf("Creare dupa distrugere reusita\n");
	}
	else
	{
		printf("Crearea dupa distrugere ESUEAZA\n");
	}
	res = LstGetNthNode(list, &node, 0);
	if (res < 0) {
		printf("NthNode pe lista goala succes\n");
	}
	else {
		printf("NthNode fail\n");
	}
	
	
	printf("---------------------------------------------\n");
	CC_VECTOR *vector = NULL;
	int res = VecCreate(&vector);
	if (res < 0) {
		printf("vector create fail");
	}
	VecInsertTail(vector, 1);
	VecInsertTail(vector, 6);
	VecInsertTail(vector, 4);
	VecInsertHead(vector, 2);
	VecInsertAfterIndex(vector, 1, 3);
	VecInsertAfterIndex(vector, 4, 5);
	printVector(vector);
	VecRemoveByIndex(vector, 0);
	printVector(vector);
	VecSort(vector);
	printVector(vector);
	int val = 0;
	VecGetValueByIndex(vector, 2, &val);
	printf("vector[2]=%d\n", val);
	printf("count=%d\n", VecGetCount(vector));
	VecClear(vector);
	VecInsertHead(vector, 1);
	printVector(vector);
	VecClear(vector);
	printf("Count pe vector gol:%d\n",VecGetCount(vector));
	printf("Insert head in vector gol:\n");
	VecInsertHead(vector, 7);
	printVector(vector);
	printf("Count pe vector cu un element:%d\n", VecGetCount(vector));
	printf("Insert head in vector cu un element, rezultat asteptat: 5 7\n");
	VecInsertHead(vector, 5);
	printVector(vector);
	VecClear(vector);
	int count;
	for (count = 1;count <= 460;count++) {
		VecInsertTail(vector, count);
	}
	printf("size dupa introducerea a 460 de elemente(0.9*512=460.8): %d\n", vector->size);
	VecInsertTail(vector, 1000);
	printf("size dupa ce factorul de umplere depaseste 0.9: %d\n", vector->size);
	VecDestroy(&vector);
	res = VecCreate(&vector);
	if (res > -1) {
		printf("Creare dupa destroy cu succes\n");
	}
	else {
		printf("CreareA dupa destroy ESUEAZA\n");
	}
	printf("-----------------------------------\n");
	
	CC_HASH_TABLE *HashTable = NULL;
	int res = HtCreate(&HashTable);
	if (res < 0) {
		printf("hashtable create fail");
	}
	HtDestroy(&HashTable);
	res = HtCreate(&HashTable);
	if (res < 0) {
		printf("Create dupa destroy esueaza!!\n");
	}
	else {
		printf("Create dupa destroy succes\n");
	}
	int val;
	res = HtGetKeyValue(HashTable, "Mama", &val);
	if (res < 0) {
		printf("Cautare in hashtable gol esueaza \n");
	}
	HtSetKeyValue(HashTable, "mama", 10);
	HtSetKeyValue(HashTable, "tata", 15);
	HtGetKeyValue(HashTable, "tata", &val);
	printf("valoarea asteptata:15, valoarea returnata:%d\n", val);
	HtSetKeyValue(HashTable, "tata", 12);
	HtSetKeyValue(HashTable, "abraclscbosiwas", 24);
	HtSetKeyValue(HashTable, "abraclscbosiwad", 24);
	HtSetKeyValue(HashTable, "asfaf", 24);
	HtGetKeyValue(HashTable, "tata", &val);
	printf("valoarea asteptata:12, valoarea returnata:%d\n", val);
	res = HtGetKeyValue(HashTable, "Mama", &val);
	if (res < 0) {
		printf("hash contine mama nu Mama \n");
	}
	res = HtHasKey(HashTable, "mama");
	else {
		printf("mama gasit\n");
	}
	res = HtHasKey(HashTable, "Mama");
	if (res < 0) {
		printf("hash contine mama nu Mama \n");
	}
	HtClear(HashTable);
	printf("key count pentru hash gol: %d\n", HtGetKeyCount(HashTable));
	char *key;
	HtSetKeyValue(HashTable, "mama", 10);
	HtSetKeyValue(HashTable, "tata", 15);
	HtSetKeyValue(HashTable, "abraclscbosiwas", 24);
	HtSetKeyValue(HashTable, "abraclscbosiwaa", 13);
	res = HtGetNthKey(HashTable, 4, &key);
	if (res < 0) {
		printf("cheia nu a fost gasita \n");
	}
	else {
		printf("la indexul 4 s'a gasit cheia %s\n",key);
	}
	HtRemoveKey(HashTable, "mama");
	res = HtGetKeyValue(HashTable, "tata", &val);
	if (res < 0) {
		printf("'tata' not found!!!\n");
	}
	HtSetKeyValue(HashTable, "tata", 24);
	HtSetKeyValue(HashTable, "mama", 12);
	HtSetKeyValue(HashTable, "cata", 42);
	HtSetKeyValue(HashTable, "mata", 1);
	HtRemoveKey(HashTable, "tata");
	HtRemoveKey(HashTable, "cata");
	HtSetKeyValue(HashTable, "rata", 39);
	HtClear(HashTable);



	printf("-------------------------------------\n");
	
	CC_VECTOR *InitialElements = NULL;
	int res = VecCreate(&InitialElements);
	if (res < 0) {
		printf("vector create fail");
	}
	//13,22,20,16,9,9,1,16 
	VecInsertTail(InitialElements, 13);
	VecInsertTail(InitialElements, 22);
	VecInsertTail(InitialElements, 20);
	VecInsertTail(InitialElements, 16);
	VecInsertTail(InitialElements, 9);
	VecInsertTail(InitialElements, 9);
	VecInsertTail(InitialElements, 1);
	VecInsertTail(InitialElements, 16);
	printf("Initial elements:");
	printVector(InitialElements);
	CC_HEAP *MinHeap = NULL;
	res = HpCreateMinHeap(&MinHeap, InitialElements);
	if (res < 0) {
		printf("min heap create fail");
	}
	printf("MinHeap:");
	printVector(MinHeap->elements);
	CC_HEAP *MaxHeap = NULL;
	res = HpCreateMaxHeap(&MaxHeap, InitialElements);
	if (res < 0) {
		printf("max heap create fail");
	}
	printf("MaxHeap:");
	printVector(MaxHeap->elements);
	HpInsert(MaxHeap, 22);
	printf("MaxHeap dupa inserarea lui 22:");
	printVector(MaxHeap->elements);
	HpInsert(MinHeap, 14);
	printf("MinHeap dupa inserarea lui 14:");
	printVector(MinHeap->elements);
	int max = 0;
	HpGetExtreme(MaxHeap, &max);
	printf("Elementul maxim din MaxHeap:%d\n", max);
	HpGetExtreme(MinHeap, &max);
	printf("Elementul maxim din MinHeap:%d\n", max);
	int extreme = 0;
	HpPopExtreme(MaxHeap, &extreme);
	printf("MaxHeap dupa pop:");
	printVector(MaxHeap->elements);
	HpPopExtreme(MinHeap, &extreme);
	printf("MinHeap dupa pop:");
	printVector(MinHeap->elements);
	CC_VECTOR *SortedVector = NULL;
	VecCreate(&SortedVector);
	HpSortToVector(MaxHeap, SortedVector);
	printf("MaxHeap sortat:");
	printVector(SortedVector);
	VecClear(SortedVector);
	printf("MinHeap sortat:");
	HpSortToVector(MinHeap, SortedVector);
	printVector(SortedVector);
	HpDestroy(&MinHeap);
	res = HpCreateMinHeap(&MinHeap, NULL);
	if (res < 0) {
		printf("Creare dupa distrugere ESUEAZA\n");
	}
	else
	{
		printf("Creare dupa stergere cu succes\n");
	}
	HpInsert(MinHeap, 3);
	HpInsert(MinHeap, 1);
	HpInsert(MinHeap, 1);
	printVector(MinHeap->elements);
	printf("Heap dupa eliminarea numarului extrem si a tuturor aparitiilor lui:");
	HpPopExtreme(MinHeap, &res);
	printVector(MinHeap->elements);
	HpInsert(MinHeap, 1);
	HpInsert(MinHeap, 4);
	HpInsert(MinHeap, 3);
	printVector(MinHeap->elements);
	printf("Heap dupa eliminarea lui 3 si a tuturor aparitiilor lui:");
	HpRemove(MinHeap, 3);
	printVector(MinHeap->elements);
	printf("-----------------------------------------\n");
	
	
	CC_TREE *Tree = NULL;
	TreeCreate(&Tree);
	TreeInsert(Tree, 13);
	TreeInsert(Tree, 10);
	TreeInsert(Tree, 15);
	TreeInsert(Tree, 5);
	TreeInsert(Tree, 11);
	TreeInsert(Tree, 16);
	TreeInsert(Tree, 4);
	TreeInsert(Tree, 8);
	printf("Arborele initial:");
	printTree(Tree);
	printf("\n");
	printf("Inseram elementul 3 pentru a testa cazul de Left-Left:\n");
	TreeInsert(Tree, 3);
	printTree(Tree);
	printf("\n");
	printf("Inseram elementul 18 pentru a testa cazul de Right-Right:\n");
	TreeInsert(Tree, 18);
	printTree(Tree);
	printf("\n");
	TreeClear(Tree);
	TreeInsert(Tree, 13);
	TreeInsert(Tree, 10);
	TreeInsert(Tree, 15);
	TreeInsert(Tree, 5);
	TreeInsert(Tree, 11);
	TreeInsert(Tree, 16);
	TreeInsert(Tree, 4);
	TreeInsert(Tree, 6);
	printf("Arborele dupa clear si refacere:");
	printTree(Tree);
	printf("\n");
	printf("Inseram elementul 7 pentru a testa cazul de Left-Right:\n");
	TreeInsert(Tree, 7);
	printTree(Tree);
	printf("\n");
	TreeClear(Tree);
	TreeInsert(Tree, 5);
	TreeInsert(Tree, 2);
	TreeInsert(Tree, 7);
	TreeInsert(Tree, 1);
	TreeInsert(Tree, 4);
	TreeInsert(Tree, 6);
	TreeInsert(Tree, 9);
	TreeInsert(Tree, 3);
	TreeInsert(Tree, 16);
	printf("Noul arbore:");
	printTree(Tree);
	printf("\n");
	printf("Inseram elementul 15 pentru a testa cazul de Right-Left:\n");
	TreeInsert(Tree, 15);
	printTree(Tree);
	printf("\n");
	printf("Nr de noduri:%d\n", TreeGetCount(Tree));
	int res = TreeContains(Tree, 7);
	if (res >= 0) {
		printf("7 gasit\n");
	}
	else {
		printf("7 NU\n");
	}
	res = TreeContains(Tree, 13);
	if (res >= 0) {
		printf("13 gasit\n");
	}
	else {
		printf("13 NU\n");
	}
	TreeClear(Tree);
	TreeInsert(Tree, 44);
	TreeInsert(Tree, 17);
	TreeInsert(Tree, 78);
	TreeInsert(Tree, 32);
	TreeInsert(Tree, 50);
	TreeInsert(Tree, 88);
	TreeInsert(Tree, 48);
	TreeInsert(Tree, 62);
	printf("Arborele pentru stergere:");
	printTree(Tree);
	printf("\n");
	TreeRemove(Tree, 32);
	printf("Arborele dupa stergerea nodului 32:");
	printTree(Tree);
	printf("\n");
	int val = 0;
	TreeGetNthInorder(Tree, 6, &val);
	printf("Index 6 in inorder este= %d\n", val);
	TreeGetNthPreorder(Tree, 2, &val);
	printf("Index 2 in preorder este= %d\n", val);
	TreeGetNthPostorder(Tree, 0, &val);
	printf("Index 0 in postorder este= %d\n", val);
	TreeDestroy(&Tree);
	TreeCreate(&Tree);
	TreeInsert(Tree, 8);
	printf("Contains pentru un arbore cu un element:\n");
	res = TreeContains(Tree, 13);
	if (res >= 0) {
		printf("13 gasit\n");
	}
	else {
		printf("13 NU\n");
	}
	res = TreeContains(Tree, 8);
	if (res >= 0) {
		printf("8 gasit\n");
	}
	else {
		printf("8 NU\n");
	}
	printf("Count pe arbore cu un element:%d\n", TreeGetCount(Tree));
	printf("Height pe arbore cu un element:%d\n", TreeGetHeight(Tree));

	TreeClear(Tree);
	printf("Contains pentru un arbore vid:\n");
	res = TreeContains(Tree, 8);
	if (res >= 0) {
		printf("8 gasit\n");
	}
	else {
		printf("8 NU\n");
	}
	printf("Count pe arbore vid:%d\n", TreeGetCount(Tree));
	printf("Height pe arbore vid:%d\n", TreeGetHeight(Tree));
	
	

	return 0;
}
void printList(CC_LIST_ENTRY *list) {
	CC_LIST_ENTRY *iterator = list->next;
	while (NULL != iterator) {
		printf("%d ", iterator->value);
		iterator = iterator->next;
	}
	printf("\n");
}
void printVector(CC_VECTOR *vector) {
	for (int i = 0;i < vector->numOfElements;i++) {
		printf("%d ", *(vector->elements + i));
	}
	printf("\n");
}
void printTree(CC_TREE* Tree){
	if (NULL == Tree) {
		return;
	}
	if (0 == Tree->height) {
		Tree = Tree->left;
	}
	printTree(Tree->left);
	printf("(%d,%d) ", Tree->key, Tree->height);
	printTree(Tree->right);
}
